#include "talia.h"

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
using namespace std;


#include "gra.h"
#include "gracz.h"
#include "karta.h"


Talia::Talia()
{
    for (int kolor = 0; kolor <= 3; kolor++) {
        for (short numer = 1; numer <= 13; numer++) {
            Talia::karty.push_back(Karta(kolor, numer));
        }
    }
}

void Talia::tasuj()
{
    srand(unsigned(time(0)));
    random_shuffle(Talia::karty.begin(), Talia::karty.end());
}

void Talia::wypisz()
{
    cout << "Karty w talii na stole:" << endl;
    for (int i = 0; i < Talia::karty.size(); i++) {
        cout << Talia::karty[i].pobierzKolor() << " " << Talia::karty[i].pobierzNumer() << "\t";
    }
    cout << endl;
}

void Talia::wypisz(Gracz &obiekt)
{
    cout << "Karty w talii gracza '" << obiekt.pobierImie() << "':" << endl;
    for (int i = 0; i < obiekt.karty.size(); i++) {
        cout << obiekt.karty[i].pobierzKolor() << " " << obiekt.
        karty[i].pobierzNumer() << "\t";
    }
    cout << endl;
}

void Talia::wydaj(Gracz &obiekt)
{
    for (int i = 0; i < 8; i++) {
        obiekt.karty.push_back(Talia::karty[i]);
    }
    Talia::karty.erase(Talia::karty.begin(), Talia::karty.begin() + 8);
}

Talia::~Talia()
{
    //dtor
}
