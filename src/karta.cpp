#include "karta.h"

Karta::Karta(short kolor, short numer) {
    Karta::kolor = kolor;
    Karta::numer = numer;
}

string Karta::pobierzKolor() {
    switch (Karta::kolor) {
    case 0:
        return "\x5";
    case 1:
        return "\x4";
    case 2:
        return "\x3";
    case 3:
        return "\x6";
    }
}

string Karta::pobierzNumer() {
    switch (Karta::numer) {
    case 1:
        return "As";
    case 11:
        return "Walet";
    case 12:
        return "Dama";
    case 13:
        return "Krol";
    }
    ostringstream ss;
    ss << Karta::numer;
    return ss.str();
}

Karta::~Karta() {
    //dtor
}
