#include "gra.h"

#include <string>
#include <iostream>
#include <vector>
using namespace std;

#include "talia.h"
#include "gracz.h"
#include "karta.h"

Gra::Gra()
{
    cout << "Witaj w grze karcianej" << endl;
    cout << "Wybierz ilu ma byc graczy (max 6):";
    cin >> Gra::ilosc_graczy;
    if (Gra::ilosc_graczy > 6 || Gra::ilosc_graczy <= 0) {
        cout << "Niestety ale podana liczba (" << Gra::ilosc_graczy << ") jest bledna!";
        return;
    }

    Gra::stworzGraczy();
    Gra::stworzTalie();
    Gra::przetasujTalie();

    Gra::wydajKarty();

    Gra::wyswietlKartyGraczy();
    Gra::wyswietlKartyNaStole();
}

void Gra::stworzGraczy()
{
    for (int i = 0; i < Gra::ilosc_graczy; i++) {
        Gra::Gracze.push_back(Gracz());
    }
}

void Gra::stworzTalie()
{
    Gra::Talia_graczy = Talia();
}

void Gra::przetasujTalie()
{
    Gra::Talia_graczy.tasuj();
}

void Gra::wydajKarty()
{
    for (int i = 0; i < Gra::ilosc_graczy; i++) {
        Gra::Talia_graczy.wydaj(Gra::Gracze[i]);
    }
}

void Gra::wyswietlKartyGraczy()
{
    for (int i = 0; i < Gra::ilosc_graczy; i++) {
        Gra::Talia_graczy.wypisz(Gra::Gracze[i]);
    }
}

void Gra::wyswietlKartyNaStole()
{
    Gra::Talia_graczy.wypisz();
}

Gra::~Gra()
{
    //dtor
}
