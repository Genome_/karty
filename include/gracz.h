#ifndef GRACZ_H
#define GRACZ_H

#include <string>
#include <iostream>
#include <vector>
using namespace std;

#include "karta.h"

class Gracz
{
    public:
        Gracz();
        virtual ~Gracz();
        string pobierImie();
        vector <Karta> karty;
    protected:
    private:
        string imie;

};

#endif // GRACZ_H
