#ifndef KARTA_H
#define KARTA_H

#include <string>
#include <iostream>
#include <vector>
#include <sstream>
using namespace std;

class Karta
{
    public:
        Karta(short kolor, short numer);
        string pobierzKolor();
        string pobierzNumer();
        virtual ~Karta();
    protected:
    private:
        short kolor; //0 - trefl \x5, 1 - karo \x4, 2 - kier \x3, 3 - pik \x6
        short numer; //1 - as 2,3,4,5,6,7,8,9,10, 11 - walet, 12 - dama, 13 - krol
};

#endif // KARTA_H
