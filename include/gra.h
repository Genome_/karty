#ifndef GRA_H
#define GRA_H

#include <string>
#include <iostream>
#include <vector>
using namespace std;

#include "talia.h"
#include "gracz.h"
#include "karta.h"

class Gra {
public:
    Gra();
    virtual ~Gra();


private:
    int ilosc_graczy;
    vector <Gracz> Gracze;
    Talia Talia_graczy;


    void stworzGraczy();
    void stworzTalie();
    void przetasujTalie();
    void wydajKarty();
    void wyswietlKartyGraczy();
    void wyswietlKartyNaStole();
};

#endif // GRA_H
